import java.util.HashMap;

public class Homework13 {



    public static void main (String[] args) {

        HashMap<Integer, String> personStreet = new HashMap<Integer, String>();

        personStreet.put(1000, "Liam");
        personStreet.put(1001, "Noah");
        personStreet.put(1002, "Olivia");
        personStreet.put(1003, "Emma");
        personStreet.put(1004, "Benjamin");
        personStreet.put(1005, "Evelyn");
        personStreet.put(1006, "Lucas");
        System.out.println(personStreet);

        System.out.println(personStreet.get(1004));

        for(int num: personStreet.keySet()) {
            if (num % 2 != 0){
                System.out.println(num + personStreet.get(num));
            }
        }
    }
}
